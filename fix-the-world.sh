#!/bin/bash

if [ ! -d ~/.local/share/npm/bin ]; then
  mkdir -p ~/.local/share/npm/bin;
  echo "prefix=/home/$USER/.local/share/npm/" > ~/.npmrc
fi

if [ ! -d ~/.local/share/go/bin ]; then
  mkdir -p ~/.local/share/go/bin;
fi

if [ ! -d ~/.local/share/rvm/bin ]; then
  mkdir -p ~/.local/share/rvm/bin;
fi

if [ ! -d ~/.local/share/go/src ]; then
  mkdir -p ~/.local/share/go/src;
fi

if [ ! -d ~/.local/share/go/pkg ]; then
  mkdir -p ~/.local/share/go/pkg;
fi

if [ ! -d ~/.local/share/java ]; then
  mkdir -p ~/.local/share/java;
fi

if [ ! -d ~/.local/bin ]; then
  mkdir -p ~/.local/bin;
fi

if [ ! -f ~/.curlrc ]; then
  echo progress-bar > ~/.curlrc
fi

if [ ! -f /etc/profile.d/vte.sh ]; then
  sudo ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh
fi